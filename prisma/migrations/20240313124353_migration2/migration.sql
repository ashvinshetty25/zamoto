/*
  Warnings:

  - You are about to drop the `MenuItem` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `Order` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `UserDetail` table. If the table is not empty, all the data it contains will be lost.

*/
-- DropTable
DROP TABLE "MenuItem";

-- DropTable
DROP TABLE "Order";

-- DropTable
DROP TABLE "UserDetail";

-- CreateTable
CREATE TABLE "userdetails" (
    "user_id" SERIAL NOT NULL,
    "user_name" TEXT NOT NULL,
    "user_pass" TEXT NOT NULL,
    "user_address" TEXT NOT NULL,
    "user_contact" TEXT NOT NULL,
    "user_email" TEXT NOT NULL,

    CONSTRAINT "userdetails_pkey" PRIMARY KEY ("user_id")
);

-- CreateTable
CREATE TABLE "menuitems" (
    "item_id" SERIAL NOT NULL,
    "item_name" TEXT NOT NULL,
    "item_description" TEXT,
    "item_imageurl" TEXT NOT NULL,
    "item_price" DECIMAL(65,30) NOT NULL,
    "item_type" TEXT NOT NULL,

    CONSTRAINT "menuitems_pkey" PRIMARY KEY ("item_id")
);

-- CreateTable
CREATE TABLE "cart" (
    "cart_id" SERIAL NOT NULL,
    "item_id" INTEGER NOT NULL,
    "item_qty" INTEGER NOT NULL,

    CONSTRAINT "cart_pkey" PRIMARY KEY ("cart_id")
);

-- CreateTable
CREATE TABLE "orders" (
    "order_id" SERIAL NOT NULL,
    "cart_id" INTEGER NOT NULL,
    "order_status" TEXT NOT NULL,
    "order_time" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "order_total" DECIMAL(65,30) NOT NULL,

    CONSTRAINT "orders_pkey" PRIMARY KEY ("order_id")
);

-- CreateTable
CREATE TABLE "delivery" (
    "delivery_id" SERIAL NOT NULL,
    "order_id" INTEGER NOT NULL,
    "d_name" TEXT NOT NULL,
    "d_contact" TEXT NOT NULL,
    "d_assignedtime" TIMESTAMP(3),
    "d_deliveredtime" TIMESTAMP(3),

    CONSTRAINT "delivery_pkey" PRIMARY KEY ("delivery_id")
);

-- AddForeignKey
ALTER TABLE "cart" ADD CONSTRAINT "cart_item_id_fkey" FOREIGN KEY ("item_id") REFERENCES "menuitems"("item_id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "orders" ADD CONSTRAINT "orders_cart_id_fkey" FOREIGN KEY ("cart_id") REFERENCES "cart"("cart_id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "delivery" ADD CONSTRAINT "delivery_order_id_fkey" FOREIGN KEY ("order_id") REFERENCES "orders"("order_id") ON DELETE RESTRICT ON UPDATE CASCADE;
