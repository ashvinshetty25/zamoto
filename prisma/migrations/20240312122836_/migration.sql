-- CreateTable
CREATE TABLE "menuitems" (
    "food_id" SERIAL NOT NULL,
    "food_name" TEXT NOT NULL,
    "food_type" TEXT NOT NULL,
    "food_price" DECIMAL(65,30) NOT NULL,
    "food_image_url" TEXT NOT NULL,

    CONSTRAINT "menuitems_pkey" PRIMARY KEY ("food_id")
);

-- CreateTable
CREATE TABLE "Order" (
    "order_id" SERIAL NOT NULL,
    "user_id" INTEGER NOT NULL,
    "food_id" INTEGER NOT NULL,
    "food_qty" INTEGER NOT NULL,
    "order_status" TEXT NOT NULL,
    "order_time" TIMESTAMP(3) NOT NULL,

    CONSTRAINT "Order_pkey" PRIMARY KEY ("order_id")
);
