/*
  Warnings:

  - You are about to drop the `menuitems` table. If the table is not empty, all the data it contains will be lost.

*/
-- DropTable
DROP TABLE "menuitems";

-- CreateTable
CREATE TABLE "MenuItem" (
    "item_id" SERIAL NOT NULL,
    "item_name" TEXT NOT NULL,
    "item_type" TEXT NOT NULL,
    "item_description" TEXT NOT NULL,
    "item_imageurl" TEXT NOT NULL,
    "item_price" DOUBLE PRECISION NOT NULL,

    CONSTRAINT "MenuItem_pkey" PRIMARY KEY ("item_id")
);
