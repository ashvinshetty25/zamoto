-- CreateTable
CREATE TABLE "UserDetail" (
    "user_id" SERIAL NOT NULL,
    "user_name" TEXT NOT NULL,
    "user_pass" TEXT NOT NULL,
    "user_address" TEXT NOT NULL,
    "user_contact" TEXT NOT NULL,
    "user_email" TEXT NOT NULL,

    CONSTRAINT "UserDetail_pkey" PRIMARY KEY ("user_id")
);
