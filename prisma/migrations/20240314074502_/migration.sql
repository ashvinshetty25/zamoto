/*
  Warnings:

  - The primary key for the `cart` table will be changed. If it partially fails, the table could be left without primary key constraint.
  - You are about to drop the column `cart_id` on the `orders` table. All the data in the column will be lost.
  - Added the required column `cart_item_id` to the `orders` table without a default value. This is not possible if the table is not empty.

*/
-- DropForeignKey
ALTER TABLE "orders" DROP CONSTRAINT "orders_cart_id_fkey";

-- AlterTable
ALTER TABLE "cart" DROP CONSTRAINT "cart_pkey",
ADD COLUMN     "cart_item_id" SERIAL NOT NULL,
ALTER COLUMN "cart_id" DROP DEFAULT,
ADD CONSTRAINT "cart_pkey" PRIMARY KEY ("cart_item_id");
DROP SEQUENCE "cart_cart_id_seq";

-- AlterTable
ALTER TABLE "orders" DROP COLUMN "cart_id",
ADD COLUMN     "cart_item_id" INTEGER NOT NULL;

-- AddForeignKey
ALTER TABLE "orders" ADD CONSTRAINT "orders_cart_item_id_fkey" FOREIGN KEY ("cart_item_id") REFERENCES "cart"("cart_item_id") ON DELETE RESTRICT ON UPDATE CASCADE;
