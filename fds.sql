--
-- PostgreSQL database cluster dump
--

-- Started on 2024-03-08 11:57:48 IST

SET default_transaction_read_only = off;

SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;

--
-- Roles
--

CREATE ROLE postgres;
ALTER ROLE postgres WITH SUPERUSER INHERIT CREATEROLE CREATEDB LOGIN REPLICATION BYPASSRLS PASSWORD 'SCRAM-SHA-256$4096:rX+aSy+x7zreiPr9VtXItA==$OJWcwBd3hDcMqN7m5p5e80Fc+C1yNyjxJrPuBeqxRqY=:o7K6fxP0QE1n3YnX3Wdkz+PeKiTwbWC4UvX/6QJxJmM=';
CREATE ROLE root;
ALTER ROLE root WITH SUPERUSER INHERIT NOCREATEROLE NOCREATEDB LOGIN NOREPLICATION NOBYPASSRLS PASSWORD 'SCRAM-SHA-256$4096:z0BRjke5YyJ3DYSXCffVAw==$R+PmDLFteyf+EVHMp/mk2HFwOgu7M0zAReeX61AiBL0=:gqSIUpWJlOCujbCDUuq4VX5kU30sszKR47hwHUcdIb8=';

--
-- User Configurations
--








--
-- Databases
--

--
-- Database "template1" dump
--

\connect template1

--
-- PostgreSQL database dump
--

-- Dumped from database version 15.5 (Ubuntu 15.5-0ubuntu0.23.04.1)
-- Dumped by pg_dump version 15.5 (Ubuntu 15.5-0ubuntu0.23.04.1)

-- Started on 2024-03-08 11:57:48 IST

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

-- Completed on 2024-03-08 11:57:48 IST

--
-- PostgreSQL database dump complete
--

--
-- Database "food-delivery-system" dump
--

--
-- PostgreSQL database dump
--

-- Dumped from database version 15.5 (Ubuntu 15.5-0ubuntu0.23.04.1)
-- Dumped by pg_dump version 15.5 (Ubuntu 15.5-0ubuntu0.23.04.1)

-- Started on 2024-03-08 11:57:48 IST

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 3392 (class 1262 OID 16389)
-- Name: food-delivery-system; Type: DATABASE; Schema: -; Owner: root
--

CREATE DATABASE "food-delivery-system" WITH TEMPLATE = template0 ENCODING = 'UTF8' LOCALE_PROVIDER = libc LOCALE = 'en_US.UTF-8';


ALTER DATABASE "food-delivery-system" OWNER TO root;

\connect -reuse-previous=on "dbname='food-delivery-system'"

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 214 (class 1259 OID 16390)
-- Name: menu_items_from_restaurant; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public.menu_items_from_restaurant (
    food_id integer NOT NULL,
    food_name text NOT NULL,
   
    food_type text NOT NULL,
    food_price numeric NOT NULL,
    food_image_url text NOT NULL
);


ALTER TABLE public.menu_items_from_restaurant OWNER TO root;

--
-- TOC entry 215 (class 1259 OID 16393)
-- Name: menu_items_from_restaurant_food_id_seq; Type: SEQUENCE; Schema: public; Owner: root
--

ALTER TABLE public.menu_items_from_restaurant ALTER COLUMN food_id ADD GENERATED BY DEFAULT AS IDENTITY (
    SEQUENCE NAME public.menu_items_from_restaurant_food_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- TOC entry 217 (class 1259 OID 16406)
-- Name: menuitems; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public.menuitems (
    food_id integer NOT NULL,
    food_name character varying(255) NOT NULL,
  
    food_type character varying(255),
    food_price numeric(10,2),
    food_image_url character varying(255)
);


ALTER TABLE public.menuitems OWNER TO root;

--
-- TOC entry 216 (class 1259 OID 16405)
-- Name: menuitems_food_id_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE public.menuitems_food_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.menuitems_food_id_seq OWNER TO root;

--
-- TOC entry 3393 (class 0 OID 0)
-- Dependencies: 216
-- Name: menuitems_food_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: root
--

ALTER SEQUENCE public.menuitems_food_id_seq OWNED BY public.menuitems.food_id;


--
-- TOC entry 3238 (class 2604 OID 16409)
-- Name: menuitems food_id; Type: DEFAULT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.menuitems ALTER COLUMN food_id SET DEFAULT nextval('public.menuitems_food_id_seq'::regclass);


--
-- TOC entry 3383 (class 0 OID 16390)
-- Dependencies: 214
-- Data for Name: menu_items_from_restaurant; Type: TABLE DATA; Schema: public; Owner: root
--

COPY public.menu_items_from_restaurant (food_id, food_name, food_qty, food_type, food_price, food_image_url) FROM stdin;
\.


--
-- TOC entry 3386 (class 0 OID 16406)
-- Dependencies: 217
-- Data for Name: menuitems; Type: TABLE DATA; Schema: public; Owner: root
--

COPY public.menuitems (food_id, food_name, food_qty, food_type, food_price, food_image_url) FROM stdin;
1	Cheeseburger	1	Burger	8.99	https://example.com/cheeseburger.jpg
2	Nariyal Chutney	1	Chutney	6.99	https://example.com/notreal.jpg
\.


--
-- TOC entry 3394 (class 0 OID 0)
-- Dependencies: 215
-- Name: menu_items_from_restaurant_food_id_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('public.menu_items_from_restaurant_food_id_seq', 1, false);


--
-- TOC entry 3395 (class 0 OID 0)
-- Dependencies: 216
-- Name: menuitems_food_id_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('public.menuitems_food_id_seq', 2, true);


--
-- TOC entry 3240 (class 2606 OID 16413)
-- Name: menuitems menuitems_pkey; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.menuitems
    ADD CONSTRAINT menuitems_pkey PRIMARY KEY (food_id);


-- Completed on 2024-03-08 11:57:48 IST

--
-- PostgreSQL database dump complete
--

--
-- Database "postgres" dump
--

\connect postgres

--
-- PostgreSQL database dump
--

-- Dumped from database version 15.5 (Ubuntu 15.5-0ubuntu0.23.04.1)
-- Dumped by pg_dump version 15.5 (Ubuntu 15.5-0ubuntu0.23.04.1)

-- Started on 2024-03-08 11:57:48 IST

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

-- Completed on 2024-03-08 11:57:48 IST

--
-- PostgreSQL database dump complete
--

-- Completed on 2024-03-08 11:57:48 IST

--
-- PostgreSQL database cluster dump complete
--

