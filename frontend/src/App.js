import AppRoutes from "./AppRoutes";
import DelHeader from "./components/Delivery/Headers/DelHeader";
import Header from "./components/Headers/Header";
import RestHeader from "./components/Restaurant/Headers/RestHeader";
function App() {

  const user = false;
  const rest = true;
  const del = false;

  let state;

  if (user) {
    state = "user";
  } else if (rest) {
    state = "rest";
  } else if (del) {
    state = "del";
  }


  return (
    <>
      {/* {rest ? <RestHeader /> : <Header />} */}
      {state === 'user' ? <Header /> : state === 'rest' ? <RestHeader /> : <DelHeader />}
      <AppRoutes />
    </>
  );
}

export default App;
