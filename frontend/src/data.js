export const foods = [
    { food_id: 520, food_name: 'Paneer Tikka Masala', food_description: 'Grilled paneer cubes in a creamy tomato-based sauce', food_imageurl: 'paneer_tikka_masala_image.jpg', food_price: 350, food_type: 'dinner', food_qty: 0 },
    { food_id: 521, food_name: 'Vegetable Biryani', food_description: 'Fragrant basmati rice cooked with mixed vegetables and spices', food_imageurl: 'vegetable_biryani_image.jpg', food_price: 300, food_type: 'lunch', food_qty: 0 },
    { food_id: 522, food_name: 'Palak Paneer', food_description: 'Creamy spinach curry with cubes of paneer', food_imageurl: 'palak_paneer_image.jpg', food_price: 280, food_type: 'dinner', food_qty: 0 },
    { food_id: 523, food_name: 'Aloo Gobi', food_description: 'Spiced potato and cauliflower curry', food_imageurl: 'aloo_gobi_image.jpg', food_price: 220, food_type: 'lunch', food_qty: 0 },
    { food_id: 524, food_name: 'Chana Masala', food_description: 'Chickpeas cooked in a spicy tomato-based sauce', food_imageurl: 'chana_masala_image.jpg', food_price: 240, food_type: 'lunch', food_qty: 0 },
    { food_id: 525, food_name: 'Baingan Bharta', food_description: 'Smoky mashed eggplant cooked with onions, tomatoes, and spices', food_imageurl: 'baingan_bharta_image.jpg', food_price: 250, food_type: 'dinner', food_qty: 0 },
    { food_id: 526, food_name: 'Dal Tadka', food_description: 'Yellow lentils tempered with spices and ghee', food_imageurl: 'dal_tadka_image.jpg', food_price: 180, food_type: 'lunch', food_qty: 0 },
    { food_id: 527, food_name: 'Pav Bhaji', food_description: 'Mixed vegetables cooked in a spicy tomato-based sauce served with buttered bread rolls', food_imageurl: 'pav_bhaji_image.jpg', food_price: 190, food_type: 'lunch', food_qty: 0 },
    { food_id: 528, food_name: 'Vegetable Korma', food_description: 'Mixed vegetables cooked in a creamy coconut-based sauce', food_imageurl: 'vegetable_korma_image.jpg', food_price: 320, food_type: 'dinner', food_qty: 0 },
    { food_id: 529, food_name: 'Masoor Dal', food_description: 'Red lentils cooked with spices and tomatoes', food_imageurl: 'masoor_dal_image.jpg', food_price: 150, food_type: 'lunch', food_qty: 0 },
    { food_id: 530, food_name: 'Mushroom Curry', food_description: 'Mushrooms cooked in a spicy gravy', food_imageurl: 'mushroom_curry_image.jpg', food_price: 270, food_type: 'dinner', food_qty: 0 },
    { food_id: 531, food_name: 'Vegetable Pulao', food_description: 'Basmati rice cooked with mixed vegetables and spices', food_imageurl: 'vegetable_pulao_image.jpg', food_price: 280, food_type: 'lunch', food_qty: 0 },
    { food_id: 532, food_name: 'Paneer Butter Masala', food_description: 'Paneer cooked in a rich tomato and butter sauce', food_imageurl: 'paneer_butter_masala_image.jpg', food_price: 320, food_type: 'dinner', food_qty: 0 },
    { food_id: 533, food_name: 'Matar Paneer', food_description: 'Paneer and peas cooked in a tomato-based sauce', food_imageurl: 'matar_paneer_image.jpg', food_price: 300, food_type: 'dinner', food_qty: 0 },
    { food_id: 534, food_name: 'Vegetable Pakora', food_description: 'Assorted vegetables dipped in chickpea batter and deep-fried', food_imageurl: 'vegetable_pakora_image.jpg', food_price: 180, food_type: 'sweet', food_qty: 0 }
];



