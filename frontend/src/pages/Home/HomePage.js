import React, { useEffect, useReducer } from 'react'
import { getAll, search } from '../../services/foodService';
import Thumbnails from '../../components/Thumbnails/Thumbnails';
import { useParams } from 'react-router-dom';
import Search from '../../components/Search/Search';
import { useNavigate } from 'react-router-dom';

const initialState={ food:[]};
const reducer = (state,action)=>{
    switch(action.type){
        case 'FOODS_LOADED':
            return {...state,foods:action.payload}
            default:
                return state;
    }
}
export default function HomePage() {
  const navigate = useNavigate();

    const [state,dispatch] = useReducer(reducer,initialState);
    const {foods} = state;
    const {searchTerm} = useParams();

    const token = localStorage.getItem("token");
    if(!token){
      localStorage.setItem("token", '');
    }
    const validateToken = async ()=>{
      if(token !== ""){
        const res = await fetch(`http://localhost:3001/user/validateToken/${token}`, {
          method:"GET",
          headers:{
            "Content-Type":"application/json"
          }
        })

        const response = await res.json();
        if(response.success == false){
          navigate('/login')
        }
      }
      else{
        navigate('/login')
      }
    }
    // useEffect(()=>{

    //   validateToken();
    // },[])

    useEffect(() => {
      validateToken();
      async function loadFoodData() {
        const loadFoods = searchTerm ? await search(searchTerm) : await getAll();
        
        // loadFoods.then(foods => 
          dispatch({ type: 'FOODS_LOADED', payload: loadFoods }) //)
          console.log({loadFoods, state})
        // .catch(error => {
        //   console.error('Error loading foods:', error);
        // });
      }
      loadFoodData()
    }, [searchTerm]);
    
    // console.log(foods);
  return <>
    {/* {console.log(foods)} */}
    {/* <Thumbnails foods={foods}/> */}
    {/* {foods.length > 0 && <Thumbnails foods={foods} />} */}
    <Search />
    {foods && foods.length > 0 && <Thumbnails foods={foods} />}
  </>
}
