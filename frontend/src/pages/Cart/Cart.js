import React, { useEffect, useState } from 'react'
import { useSelector } from 'react-redux'
import Title from '../../components/Title/Title'
import classes from './cart.module.css'
import { Link } from 'react-router-dom'
import { deleteItems as deleteItemsAction, addQuantity as addQuantityAction, deleteQuantity as deleteQuantityAction } from '../../redux/reducer/cartSlice'
import { useDispatch } from 'react-redux';

export default function Cart() {
  const dispatch = useDispatch();
  const cart = useSelector((state) => state.cart)
  const [food, setFood] = useState([]);
  // const [foodQty, setfoodQty] = useState([]);

  useEffect(() => {
    const loadCartFromLocalStorage = () => {
      const storedCart = localStorage.getItem('cart');
      if (storedCart) {
        setFood(JSON.parse(storedCart));
      }
      console.log({cart, storedCart})
    };
    loadCartFromLocalStorage();
  }, []);
  const handleRemoveItem = (item) => {
    dispatch(deleteItemsAction(item));
    setFood(prevFood => prevFood.filter(foodItem => foodItem.food_id !== item.food_id));
    const existingCart=localStorage.getItem('cart')
    let cart = existingCart ? JSON.parse(existingCart) : [];
    console.log(item);
    const updatedCart=cart.filter((food)=>food.food_id!==item.food_id)
    localStorage.setItem('cart', JSON.stringify(updatedCart));
  };

  // const handleIncreaseQuantity = (item) => {
  //   dispatch(addQuantityAction({ food_id: item.food_id, food_qty: 1 }));
  //   console.log(typeof item.food_price); 
  //   console.log(typeof item.food_qty); 
  //  };

  // const handleDecreaseQuantity = (item) => {
  //   if (!item.food_id !== 0) {
  //     dispatch(deleteQuantityAction({ food_id: item.food_id, food_qty: 1 }));
  //   }
  // };
  // const handleRemoveItem = (item) => {
  //   const updatedCart = food.filter((foodItem) => foodItem.food_id !== item.food_id);
  //   setFood(updatedCart);
  //   localStorage.setItem('cart', JSON.stringify(updatedCart));

  // };

  const handleIncreaseQuantity = (item) => {
    console.log(item);  
    const updatedCart = food.map((foodItem) =>
      foodItem.food_id === item.food_id ? { ...foodItem, food_qty: foodItem.food_qty + 1 } : foodItem
    );
    setFood(updatedCart);
    localStorage.setItem('cart', JSON.stringify(updatedCart));
  };

  const handleDecreaseQuantity = (item) => {
    if(item.food_qty <= 1){
      handleRemoveItem(item);
      return; 
    }
    const updatedCart = food.map((foodItem) =>
      foodItem.food_id === item.food_id //&& foodItem.food_qty > 1
        ? { ...foodItem, food_qty: foodItem.food_qty - 1 }
        : foodItem
    );
    console.log('handleDescrease:' , {updatedCart})
    // const temp = updatedCart.filter(food => food.food_id == item.food_id)
    // if(temp[0].food_qty < 1){
    //   handleRemoveItem(item);
    //   console.log('removedItem'); 
    // }else{
      setFood(updatedCart);
      localStorage.setItem('cart', JSON.stringify(updatedCart));
    // }
  };

  const handleOrder=async ()=>{
    const response = await fetch('http://localhost:3001/cart/getMaxCartId',{
        method:'GET',
        headers:{
          'Content-Type':'application/json',
        }
    } )
    const respJson = await response.json()
    const {getMaxCartId} = respJson;
    // console.log(respJson.getMaxCartId);
    food.forEach( async (food)=>{
      console.log({cart_id: getMaxCartId+1, food_id: food.food_id , food_qty: food.food_qty});

   
    const res= await fetch('http://localhost:3001/cart/addItemInCart',{
      method:'POST',
      headers: {
      'Content-Type':'application/json',
      },
      body:
        JSON.stringify({
          cart_id:getMaxCartId+1,
          item_id: food.food_id , 
          item_qty: food.food_qty


        })
   })
   console.log(res);
  })
  }
    console.log({food});
  const totalPrice = food.reduce((acc, item) => acc + item.food_price * item.food_qty, 0);

  return (
    <>
      <Title title="Your Items.." margin="1.5rem 0 0 2.5rem" />

      {/* {cart && cart.length > 0 &&
        <div className={classes.container}>
          <div className={classes.list}>
            {cart.map(item => <li key={item.food_id}>
              <div>
                <img className={classes.image} src={`/foods/${item.food_imageurl}`} alt={item.food_name} />
              </div>
              <div>
                <Link to={`/food/${item.food_id}`}>{item.food_name}</Link>
              </div>
              <div className={classes.buttons}>
                <button onClick={() => {
                  if (item.food_qty === 1) {
                    handleRemoveItem(item)
                  }
                  else if (item.food_qty > 0) {
                    handleDecreaseQuantity(item)
                  } else {
                    handleRemoveItem(item)
                  }
                }}> - </button>
                <span>{item.food_qty}</span>
                <button onClick={() => handleIncreaseQuantity(item)}> + </button>
              </div>
              <div>
                ₹{
                // console.log(item.food_price,item.food_qty)
                (item.food_price * item.food_qty)}
              </div>
              <div>
                <button className={classes.removebutton} onClick={() => handleRemoveItem(item)}>Remove</button>
              </div>
            </li>)}
          </div>
          <div className={classes.checkout}>
            <div className={classes.foodcount}>Count = </div>
            <div className={classes.totalprice}>Total Price = { }</div>
          </div>
          <Link to="/checkout">Order</Link>
        </div>
      } */}
        {food && food.length > 0 && 
        <div className={classes.container}> 
          <div className={classes.list}>
            {food.map(item => ( 
              <li key={item.food_id}> 
                <div>
                  <img className={classes.image} src={`/foods/${item.food_imageurl}`} alt={item.food_name} /> 
                </div>
                <div>
                  <Link to={`/food/${item.food_id}`}>{item.food_name}</Link> 
                </div>
                <div className={classes.buttons}>
                  <button onClick={() => handleDecreaseQuantity(item)}> - </button> 
                  <span>{item.food_qty}</span> 
                  <button onClick={() => handleIncreaseQuantity(item)}> + </button> 
                </div>
                <div>
                  ₹{item.food_price * item.food_qty} 
                </div>
                <div>
                  <button className={classes.removebutton} onClick={() => handleRemoveItem(item)}>Remove</button> 
                </div>
              </li>
            ))}
          </div>
          <div className={classes.checkout}>
            <div className={classes.foodcount}>Count = {food.length}</div> 
            <div className={classes.totalprice}>Total Price = { totalPrice}</div> 
          </div>
          {/* <Link to="/checkout">Order</Link>  */}
          <button onClick={(e) => {
            console.log("handleorder"); handleOrder(e)}}>Place Order</button>
        </div>
      }
    </>
  )
}

