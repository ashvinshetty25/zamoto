import React, { useEffect, useState } from 'react'
import { useParams } from 'react-router-dom'
import classes from './food.module.css'
import { getById } from '../../services/foodService';
import { addItems as addItemsAction } from '../../redux/reducer/cartSlice';
import { useDispatch, useSelector } from 'react-redux';

export default function FoodPage() {
  const [food, setFood] = useState({});
  const { id } = useParams();

  const dispatch = useDispatch();
  const cart = useSelector(state => state.cart || [])
  const isInCart = cart.some(item => item.food_id === food.food_id);

  useEffect(() => {
    const fetchData = async () => {
      try { 
        const foodData = await getById(Number(id));
        setFood(foodData);
        console.log(foodData);
        console.log({
          id, cart, isInCart, food, foodData
        })
      } catch (error) {
        console.error('Error loading food:', error);
      }
    };
    fetchData();
  }, [id]);

  function addItems() {
    food.food_qty = 1;
    dispatch(addItemsAction((food)));
    // const existingCart=localStorage.getItem('cart')
    // let cart = existingCart ? JSON.parse(existingCart) : [];
    // cart.push(food);
    // localStorage.setItem('cart', JSON.stringify(cart));
  }

  return <>
    {food && <div className={classes.container}>
      <img className={classes.image} src={`/foods/${food.food_imageurl}`} alt={food.food_name} />
      <div className={classes.details}>
        <div className={classes.header}>
          <span className={classes.name}>
            {food.food_name}
          </span>
          <span className={classes.description}>{food.food_description}</span>
          <span className={classes.price}>Price: ₹{food.food_price}</span>
          <div className={classes.button}>{!isInCart && <button onClick={() => {
            addItems();
          }}>Add to cart</button>}
            {isInCart && <span>Added to cart</span>}
          </div>
        </div>
      </div>
    </div>}
  </>
}

// export default function FoodPage() {
//     const [food, setFood] = useState({});
//     const { id } = useParams();

//     console.log('FoodPage - Before useEffect: ', food);

//     useEffect(() => {
//         console.log('FoodPage - Inside useEffect');
//         getById(id)
//             .then((data) => {
//                 console.log('FoodPage - Data fetched: ', data);
//                 setFood(data);
//             })
//             .catch(error => console.error('Error fetching food:', error));
//     }, [id]);

//     console.log('FoodPage - After useEffect: ', food);

//     return (
//         <div className={classes.container}>
//             {food && food.food_imageurl && <img src={`/food/${food.food_imageurl}`} alt="Food" />}
//         </div>
//     );  
// }
