// LogoutButton.js

import React from 'react';
import { useDispatch } from 'react-redux';
import { logout } from '../reducer/UserSlice';

const LogoutButton = () => {
  const dispatch = useDispatch();

  const handleLogout = () => {
    dispatch(logout()); // Dispatch the logout action
    localStorage.removeItem('token'); // Remove the token from local storage
  };

  return (
    <button onClick={handleLogout}>Logout</button>
  );
};

export default LogoutButton;
