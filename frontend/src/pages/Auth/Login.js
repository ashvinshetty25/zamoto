import React, { useState } from 'react';
import { useNavigate } from "react-router-dom";
import '../../Login.css'
import { useSelector, useDispatch } from 'react-redux';
// import { useDispatch } from 'react-redux';
import { login } from '../../redux/reducer/UserSlice';
const LoginForm = () => {
    const [email, setEmail] = useState('rachit@gmail.com');
    const [password, setPassword] = useState('3333');
    const [errorMessage, setErrorMessage] = useState('');
    const navigate = useNavigate();

    const dispatch = useDispatch();
    const handleSubmit = async (e) => {

        e.preventDefault();
        try {

            const resp = await fetch('http://192.168.10.248:3001/user/Login', {
                method: 'POST',
                headers: {
                    "Content-Type": "application/json"
                },
                body: JSON.stringify({
                    email,
                    password
                })
            })
            const data = await resp.json();
            console.log(data.user.user_name);
            if (data.status === 200) {
                localStorage.setItem('token', data.token)
                dispatch(login(data.user))
                //save user details in context api / redux
                navigate('/');

            }
            else {
                console.log(data.message);
                setErrorMessage(data.message)
            }
            // dispatch(login());
        } catch (error) {
            console.log(error);
        }

        // Perform validation if needed
        // onLogin({ username, password });
    };

    return (
        <div className="form-container">
            <h2>Login</h2>
            <form onSubmit={handleSubmit}>
                <div className="form-group">
                    <label>Email:</label>
                    <input type="text" value={email} required={true} onChange={(e) => setEmail(e.target.value)} />
                </div>
                <div className="form-group">
                    <label>Password:</label>
                    <input type="password" value={password} required={true} onChange={(e) => setPassword(e.target.value)} />
                </div>
                <button type="submit">Login</button>
                {errorMessage && <div style={{ color: 'red' }}>{errorMessage}</div>}

            </form>
        </div>
    );
};

export default LoginForm;
