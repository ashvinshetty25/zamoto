import react, { useState, useEffect } from 'react';
// import { useNavigate } from "react-router-dom";
import '../../SignUp.css';
import { useNavigate } from 'react-router-dom';
function SignUp() {
    const [userName, setUserName] = useState('');
    const [userPass, setUserPass] = useState('');
    const [userEmail, setUserEmail] = useState('');
    const [userAddress, setUserAddress] = useState('');
    const [userContact, setUserContact] = useState('');
    let navigate = useNavigate();
    const handleSubmit = async (e) => {
        e.preventDefault();
        try {
            const response = await fetch('http://localhost:3001/user/addUser', {
                headers: {
                    "Content-Type": "application/json" 
                },
                method: "POST",
                body: JSON.stringify({
                    user_name: userName,
                    user_pass: userPass,
                    user_address: userAddress,
                    user_contact: userContact,
                    user_email: userEmail
                })
            });
            if (!response.ok) {
                throw new Error('Failed to add user'); 
            }
            console.log('User added successfully');
           
            setUserName('');
            setUserPass('');
            setUserEmail('');
            setUserAddress('');
            setUserContact('');

            navigate('/login')
        } catch (error) {
            console.error('Error adding user:', error.message);
            // Handle error state or display error message to the user
        }
    }
        return (
            <div className="user-form-container">
                <form onSubmit={handleSubmit}>
                    <div className="form-row">
                        <div className="form-group">
                            <label>User Name:</label>
                            <input type="text" value={userName} required={true} onChange={(e) => setUserName(e.target.value)} />
                        </div>
                        <div className="form-group">
                            <label>Password:</label>
                            <input type="password" value={userPass}  required={true}onChange={(e) => setUserPass(e.target.value)} />
                        </div>
                    </div>
                    <div className="form-row">
                        <div className="form-group">
                            <label>Address:</label>
                            <input type="text" value={userAddress} required={true} onChange={(e) => setUserAddress(e.target.value)} />
                        </div>
                        <div className="form-group">
                            <label>Contact:</label>
                            <input type="text" value={userContact}required={true} onChange={(e) => setUserContact(e.target.value)} />
                        </div>
                    </div>
                    <div className="form-row">
                        <div className="form-group">
                            <label>Email:</label>
                            <input type="email" value={userEmail} required={true} onChange={(e) => setUserEmail(e.target.value)} />
                        </div>
                    </div>
                    <button  className="form-button" type="submit" >sign Up</button>
                </form>
            </div>

        )
    }

    export default SignUp;


