// import { createSlice } from "@reduxjs/toolkit";

// const initialState = [];

// export const cartSlice = createSlice({
//     name: 'cart',
//     initialState: initialState,
//     reducers: {
//         addItems(state, action) {
//             state.push(...state,action.payload);
//         }
//     }
// })

// export const {addItems} = cartSlice.actions;
// export default cartSlice.reducer

import { createSlice } from "@reduxjs/toolkit";

const initialState = JSON.parse(localStorage.getItem('cart')) ?? [];

export const cartSlice = createSlice({
    name: 'cart',
    initialState: initialState,
    reducers: {
        addItems(state, action) {
            const newItem = { ...action.payload, food_qty: 1 }
            return [...state, newItem];
        },
        addCart(state, action){
            return action.payload;
        },
        deleteItems(state, action) {
            return state.filter((item) => item.food_id !== action.payload.food_id);
        },
        addQuantity(state, action) {
            const { food_id, food_qty } = action.payload;
            return state.map(item => item.food_id === food_id ? { ...item, food_qty: item.food_qty + food_qty } : item);
        },
        deleteQuantity(state, action) {
            const { food_id, food_qty } = action.payload;
            return state.map(item => item.food_id === food_id ? { ...item, food_qty: item.food_qty - food_qty } : item);
        }
    }
})

export const { addItems } = cartSlice.actions;

export const { addCart } = cartSlice.actions;

export const { deleteItems } = cartSlice.actions;

export const { addQuantity } = cartSlice.actions;

export const { deleteQuantity } = cartSlice.actions;

export default cartSlice.reducer