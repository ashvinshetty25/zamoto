const initialState = {
    user: null
};

const userReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'add':
            return { ...action.payload }; // Copying action.payload into a new state object
        case 'clear':
            return {}; // Returning a new empty state object
        default:
            return state;
    }
}

export default userReducer;
