import { configureStore } from '@reduxjs/toolkit';
// import userReducer from './reducer/user';
import userSliceReducer from './reducer/UserSlice';
import cartSliceReducer from './reducer/cartSlice';


const store = configureStore({
    reducer: {
      userSlice: userSliceReducer,
      cart: cartSliceReducer
    }
  });

store.subscribe(() => {
  const cart = store.getState().cart ?? [];
  // console.log({cart})
  localStorage.setItem('cart', JSON.stringify(cart));
  
})

export default store;