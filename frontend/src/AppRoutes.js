import React from 'react'
import { Route, Routes } from 'react-router-dom'
import HomePage from './pages/Home/HomePage'
import FoodPage from './pages/Food/FoodPage'
import LoginForm from './pages/Auth/Login'
import SignUp from './pages/Auth/SignUp'
import Cart from './pages/Cart/Cart'
import RestHome from './pages/Restaurant/Home/RestHome'
import Orders from './pages/Orders/Orders'
import DeliveryHome from '../src/pages/Delivery/DeliveryHome'
export default function AppRoutes() {


  const user = false;
  const rest = true;
  const del = false;

  let state;

  if (user) {
    state = "user";
  } else if (rest) {
    state = "rest";
  } else if (del) {
    state = "del";
  }


  return <Routes>

    {state === 'user' ? <Route path='/' element={<HomePage />} /> : state === 'rest' ? <Route path='/' element={<RestHome />} /> : <Route path='/' element={<DeliveryHome />} />}
    {/* <Route path="/" element={<HomePage />} /> */}
    <Route path="/search/:searchTerm" element={<HomePage />} />
    <Route path='/food/:id' element={<FoodPage />} />
    <Route path='/login' element={<LoginForm />} />
    <Route path='/signup' element={<SignUp />} />
    <Route path='/cart' element={<Cart />} />


    {/* Restaurant routes */}
    {/* <Route path='/rest/home' element={<RestHome />} /> */}

  </Routes>
}
