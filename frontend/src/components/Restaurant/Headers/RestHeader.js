import React, { useCallback, useEffect, useState } from 'react'
import classes from './RestHeader.module.css'
import { Link } from 'react-router-dom';
import { useNavigate } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux'; // Import useDispatch
import { logout, login } from '../../../redux/reducer/UserSlice';

export default function RestHeader() {

    const dispatch = useDispatch();
    const navigate = useNavigate();
    const cart = {
        totalCount: 0
    };
    const cartCount = useSelector((state) => state.cart);
    const cartCount2 = localStorage.getItem('cart')
    const user = useSelector((state) => state.userSlice.user)
    //   console.log(user);

    const logout1 = useCallback(() => {
        dispatch(logout());
        localStorage.setItem('token', '');
        navigate('/');
    }, [navigate]);

    const validateToken = async (token) => {
        console.log('inside function validate ');
        try {
            const res = await fetch(`http://localhost:3001/user/validateToken/${token}`, {
                method: 'GET',
                headers: {
                    "Content-Type": "application/json"
                }
            });
            const data = await res.json();
            // console.log(data);
            if (data.success == true) {
                dispatch(login(data.user));
            } else {
                dispatch(logout());
            }
        }
        catch (error) {
            console.log('Error validating token:', error);
            dispatch(logout());
        }
    };

    //
    useEffect(() => {
        const token = localStorage.getItem("token");
        console.log(token);
        // const validateToken = async (token)=>{
        //   const res = await fetch(`http://localhost:3001/user/validateToken/${token}`);
        //   const data = await res.json();
        //   // initialState.user = data.user;
        //   userDetails = data.user;
        // }
        if (token != '') {
            validateToken(token);
        }
    }, [])



    return (
        <header className={classes.header}>
            <div className={classes.container}>
                <Link to="/" className={classes.logo}>
                    ZAMOTO |Restaurant-Partner|
                </Link>
                <nav>
                    <ul>
                        {
                            user ?
                                <li className={classes.menu_container}>
                                    <Link>{user.user_name}</Link>
                                    <div className={classes.menu}>
                                        <Link to="/profile">Profile</Link>
                                        <Link to="/orders">Orders</Link>

                                        <a onClick={logout1}>Logout</a>
                                    </div>
                                </li> :
                                <li>
                                    <Link to="/login">Login</Link>
                                    <Link to="/signup" >SignUp</Link>

                                </li>
                        }
                    </ul>
                </nav>
            </div>
        </header>
    )
}
