import React, { useCallback, useEffect, useState } from 'react'
import classes from './header.module.css'
import { Link } from 'react-router-dom';
import { useNavigate } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux'; 
import {addCart} from '../../redux/reducer/cartSlice'
import { logout, login } from '../../redux/reducer/UserSlice';
export default function Header() {
    const [cartCount,setCartCount]=useState('');
    const dispatch = useDispatch();
    const navigate = useNavigate();
    const cart = {
        totalCount: 0
    };
    const cartCountresp=localStorage.getItem('cart')
    const cartArray =JSON.parse(cartCountresp) || [];
    // setCartCount(cartArray.length)
    // console.log(cartArray , cartArray.length);
    const cartLength = cartArray.length;
    const cartCountFromStore=useSelector((state)=>state.cart);
  console.log(cartCountFromStore.length);
  const value=cartCountFromStore.length;
  const user = useSelector((state)=>state.userSlice.user)
//   console.log(user);
  
  const logout1 =useCallback(()=>{
    dispatch(logout());
    localStorage.setItem('token', '');
    navigate('/');
  },[ navigate]);

  const validateToken = async (token) => {
    console.log('inside function validate ');
    try {
        const res = await fetch(`http://localhost:3001/user/validateToken/${token}`,{
            method:'GET',
            headers:{
                "Content-Type": "application/json"
            }
        });
        const data = await res.json();
        // console.log(data);
        if (data.success == true) {
          dispatch(login(data.user));
        } else {
          dispatch(logout());
        }
      } 
     catch (error) {
      console.log('Error validating token:', error);
      dispatch(logout());
    }
  };
  
  //
  useEffect(()=>{
      const token = localStorage.getItem("token");
      console.log(token);
      // const validateToken = async (token)=>{
      //   const res = await fetch(`http://localhost:3001/user/validateToken/${token}`);
      //   const data = await res.json();
      //   // initialState.user = data.user;
      //   userDetails = data.user;
      // }
      if(token != ''){
        validateToken(token);
      }
  },[]);
  useEffect(()=>{
    // const localCartData  = JSON.parse(localStorage.getItem('cart'));
    // dispatch(addCart(localCartData))
  },[])
  

    return (
        <header className={classes.header}>
            <div className={classes.container}>
                <Link to="/" className={classes.logo}>
                    ZAMOTO
                </Link>
                <nav>
                    <ul>
                        {
                            user ?
                                <li className={classes.menu_container}>
                                    <Link>{user.user_name}</Link>
                                    <div className={classes.menu}>
                                        <Link to="/profile">Profile</Link>
                                        <Link to="/orders">Orders</Link>

                                        <a onClick={logout1}>Logout</a>
                                    </div>
                                </li> :
                                <li>
                                    <Link to="/login">Login</Link>
                                    <Link to="/signup" >SignUp</Link>

                                </li>
                        }
                        <li className={classes.cart}>
                            <Link to="/cart">
                                <span className={classes.cart_text}>Cart</span>
                                {/* {cart.totalCount > 0 && <span className={classes.cart_count}>{cart.totalCount}</span>} */}
                                {cartLength>0 && <span className={classes.cart_count}>{value}</span>}
                            </Link>
                        </li>
                    </ul>

                </nav>
            </div>
        </header>
    )
}
