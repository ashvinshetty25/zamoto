import React from 'react'
import classes from './thumbnails.module.css'
import { Link } from 'react-router-dom';

export default function Thumbnails( {foods} ) {
    return ( 
    <ul className={classes.list}>
      {
        foods.map((food, idx) =>
          <li key={food.food_id} className={classes.listparent}>
            <Link to={`/food/${food.food_id}`} className={classes.imageContainer}>
              <img className={classes.image}
                src={`foods/${food.food_imageurl}`}
                alt={food.food_name}
              />
            </Link>
            <div className={classes.content}>
              <div className={classes.name} >
                {food.food_name}
              </div>
              <div className={classes.description}>
                {food.food_description}
              </div>
              <div className={classes.price}>₹
                {food.food_price}
              </div>
            </div>
          </li>
        )
      }
    </ul>
  )
}
