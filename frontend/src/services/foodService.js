// import { foods } from "../data";

export const getAll = async () => {
  const data = await fetch('http://192.168.10.248:3001/menu/menuitems');
  const dataJson = data.json()
  return dataJson;
  // return new Promise((resolve, reject) => {
  //   try {
  //     resolve(foods);
  //   } catch (error) {
  //     reject(error);
  //   }
  // });
};

export const search = async (searchTerm) => {
  const data = await fetch('http://192.168.10.248:3001/menu/menuitems');
  const dataJson = data.json()
  dataJson.filter(item => item.food_name.toLowerCase().includes(searchTerm.toLowerCase()))
}

export const getById = async (foodId) => {
  const data = await fetch('http://192.168.10.248:3001/menu/menuitems/' + foodId);
  const dataJson = data.json()
  // return dataJson;
  return dataJson; //dataJson.find(item=>item.food_id === foodId);
};


