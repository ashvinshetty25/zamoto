const { PrismaClient } = require('@prisma/client');

const prisma = new PrismaClient();
require('dotenv').config();



async function main() {
  try {
    const newMenuItem = await prisma.menuitems.create({
      data: {
        food_name: 'Nariyal Chutney',
        
        food_type: 'Chutney',
        food_price: 6.99,
        food_image_url: 'https://example.com/notreal.jpg',
      }
    });
    console.log('Created new menu item:', newMenuItem);
  } catch (error) {
    console.error('Error adding menu item:', error);
  } finally {
    await prisma.$disconnect();
  }
}

// main();
