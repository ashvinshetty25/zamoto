const express=require('express')
const router=express.Router();
const { PrismaClient } = require('@prisma/client');
const path = require('path');
const { log } = require('console');
require('dotenv').config({ path: path.resolve(__dirname, '../.env') });

const prisma = new PrismaClient();

router.post('/addOrder',async (req,res)=>{
    try {
        const {cart_id,order_status}=req.body;

        if(!cart_id || !order_status){
            return res.status(400).json({error:"missing reuired feild"})
        }
        const cartItemId=await prisma.cart.findMany({
            where:{cart_id:cart_id},
            include:{
                menuitems:{
                    select:{
                        item_price:true,
                        item_name:true

                    }
                }
            }
        })
        var order_total=0

        cartItemId.forEach(item=>{
            order_total=order_total+item.item_qty*item.menuitems.item_price
        })
        console.log(cartItemId.length);
        if(!cartItemId.length>0){
            return res.status(400).json({error:"cart_id is not valid"})
        }
        const newOrder = await prisma.orders.create({
            data: {
                cart: { connect: { cart_item_id: cartItemId[0].cart_item_id } }, 
                order_status,
                order_total:parseInt(order_total),
                order_time: new Date()
            }
          });
          const allCartItems = cartItemId.map(item => ({
        name: item.menuitems.item_name,
        quantity: item.item_qty,
        item_price: parseInt(item.menuitems.item_price),
        total_price: (item.item_qty * item.menuitems.item_price) 
    }))
          const resp={
            msg:"order place succesfully",
            items:allCartItems,
            order_total:order_total
          }
         
          console.log(resp);
          res.status(201).json(resp);
    } catch (error) {
        console.log(error);
        res.json({error:"order failed"});
    }
   

        // Create new order
     
     
})
// get all order 
router.get('/allOrder',async(req,res)=>{
    // res.send("hii");
    try {
        const orderItem = await prisma.orders.findMany({
            include:{
                cart:{
                    select:{
                        cart_id:true
                    }
                }
            }
        });
        const orderResp=orderItem.map(order=>({
            order_id:order.order_id,
            order_status:order.order_status,
            cart_id:order.cart.cart_id,
            order_time:order.order_time,
            order_total:order.order_time

            
        }))

        console.log(orderResp)
        res.json(orderResp);
      } catch (error) {
        console.error('Error fetching menu items:', error);
        res.status(500).json({ error: 'Internal Server Error' });
      }
   
})
// get single order
router.get('/singleOrder/:order_id',async (req,res)=>{
    try{
        const order=await prisma.orders.findFirst({
            where:{order_id:parseInt(req.params.order_id)}
        })
        console.log(order);
        res.json(order)
    }catch(error){
        console.log(error);
    }
   
})
router.put('/updateOrder/:order_id',async(req,res)=>{
    try {
        const {cart_item_id,order_status,order_total}=req.body;
        const updatedItem= await prisma.orders.update({
            where:{order_id:parseInt(req.params.order_id)},
            data:{
                cart_item_id,order_status,order_total,order_time:new Date()
            }
        });
        console.log(updatedItem);
        res.status(201).json(updatedItem)
    } catch (error) {
        
    }
})
router.delete('/deleteOrder/:order_id', async(req,res)=>{
    try {
        const deletedOrder= await prisma.orders.delete({
            where:{order_id:parseInt(req.params.order_id)}
          })
    
          res.json(deletedOrder);
    } catch (error) {
        console.log(error);
    }
     
})

module.exports=router;