const express=require('express')
const router=express.Router();
const { PrismaClient } = require('@prisma/client');
const path = require('path');
const jwt = require('jsonwebtoken')
const dotenv = require("dotenv");

dotenv.config();

// require('dotenv').config({ path: path.resolve(__dirname, '../.env') });

const prisma = new PrismaClient();


router.post('/addUser',async (req,res)=>{
    try {
        const {user_name ,user_pass ,user_address ,user_contact ,user_email }=req.body ;
        if(!user_name || !user_pass || !user_address || !user_contact || !user_email){
            return res.status(400).json({error:"missing required feild"})
        }
        const userDetail= await prisma.userdetails.create({
         data:{
             user_name,
             user_pass,
             user_address,
             user_contact,
             user_email
         }
        })
        console.log(userDetail);
        res.status(201).json({message:`${userDetail.user_name} added succesfully`,userDetail});
    } catch (error) {
        console.log(error);
        res.send(error);
    }
 
})

router.put('/updateUser/:user_id',async (req,res)=>{
    try{

    
    const {user_name,user_pass,user_address,user_contact,user_email}=req.body;
    const updatedUser=await prisma.userdetails.update({
        where:{user_id:parseInt(req.params.user_id)},
        data:{
            user_name,user_pass,user_address,user_contact,user_email
        }
    })
    console.log(updatedUser);
    res.status(201).json(updatedUser)
    }catch(error){
        console.log(error);
    }
})
// get All User
router.get('/getUsers',async(req,res)=>{
    try {
        const userDetail=await prisma.userdetails.findMany();
        console.log(userDetail);
        res.status(201).json(userDetail);
    } catch (error) {
        console.log(error);
    }

})
// get single user
router.get('/getUser/:user_id',async(req,res)=>{
    try {
        const id=req.params.user_id;
        const userDetail=await prisma.userdetails.findFirst({
            where:{user_id:parseInt(id)}
        })
        console.log(userDetail);
        res.status(201).json(userDetail);
    } catch (error) {
        console.log(error);
    }
    

})

router.delete('/deleteUser/:user_id',async (req,res)=>{
    try {
        const id= req.params.user_id;
        const deletedUser=await prisma.userdetails.delete({
           where:{user_id:parseInt(id)}
        })
        res.json(deletedUser);
    } catch (error) {
        console.log(error);
    }
   
})

// router.post('/login', async (req, res) => {
//     try {
//         console.log('hii');
      
//         const { email, password } = req.body;
//         // console.log(username, password);
//         if(!email || !password){
//             return res.status(401).json({ message: 'email or password required' });
//         }
       
//         const user = await prisma.userdetails.findFirst({
//             where: {
//                 user_email:email,
//                 user_pass:password,
//             },
//         });    
//         if (user) {          
//             return res.status(200).json({ status: 200,message: 'Login successful', user });
//         } else {
        
//             return res.status(401).json({ status: 401,message: 'Invalid credentials' });
//         }
//     } catch (error) {
//         console.error('Error during login:', error);
//         return res.status(500).json({message: 'Internal server error' });
//     }
// });

router.post('/login', async (req, res) => {
    try {
        const { email, password } = req.body;

        if (!email || !password) {
            return res.status(401).json({ message: 'Email or password required' });
        }

        // Authenticate user
        const user = await prisma.userdetails.findFirst({
            where: {
                user_email: email,
                user_pass: password,
            },
        });

        if (!user) {
            return res.status(401).json({ message: 'Invalid credentials' });
        }

        // Generate JWT token
        const token = jwt.sign(user, process.env.SECRET_KEY);
        console.log(token);
        res.status(200).json({ status: 200, message: 'Login successful', token, user });
    } catch (error) {
        console.error('Error during login:', error);
        return res.status(500).json({ message: 'Internal server error' });
    }
});
router.get('/validateToken/:token',(req,res)=>{
    const tokenFromClient=req.params.token;
    if(tokenFromClient == ""){
        return res.status(401).json({message: 'invalid token', success: false})
    }
    try {
        const user = jwt.verify(tokenFromClient, process.env.SECRET_KEY);
        // console.log(user);
        if(!user){
            return res.status(401).json({message: 'invalid token', success: false})
        }
        return res.status(201).json({message: 'token verified', success: true, user: user})
    } catch (error) {
        if (error instanceof jwt.JsonWebTokenError) {
            console.error('JWT Error:', error.message);
            return res.status(401).json({ message: 'Invalid token', success: false });
        } else if (error instanceof jwt.TokenExpiredError) {
            console.error('Token Expired:', error.message);
            return res.status(401).json({ message: 'Token expired', success: false });
        } else {
            console.error('Error:', error.message);
            return res.status(500).json({ message: 'Internal server error', success: false });
        }
    }
    // if(tokenFromClient!=process.env.SECRET_KEY)


})
module.exports=router;