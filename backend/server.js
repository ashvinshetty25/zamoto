const express = require('express')
const app = express();
const menuRouter = require('./menuRoutes.js');
const OrderRoute = require('./OrderRoutes.js');
const userRoutes = require('./userRoutes')
const cartRoutes = require('./cartRoutes.js')
const dotenv = require("dotenv")
dotenv.config();
const cors = require('cors')
app.use(cors());
app.use(express.json());
app.use('/menu', menuRouter);
app.use('/order', OrderRoute);
app.use('/user', userRoutes);
app.use('/cart', cartRoutes)

const port = process.env.PORT || 3001;


app.listen(port, () => {
    console.log(`server is running at 
   http://localhost:${port}/`);
    ``
});