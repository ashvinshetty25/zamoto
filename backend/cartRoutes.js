const express=require('express')
const router=express.Router()
const { PrismaClient } = require('@prisma/client');
const path = require('path');

require('dotenv').config({ path: path.resolve(__dirname, '../.env') });

const prisma = new PrismaClient();
// add item in cart

router.post('/addItemInCart',async (req,res)=>{
    try {
        const {cart_id,item_id,item_qty}=req.body;
        console.log({body: req.body})

        if(!cart_id || !item_id || !item_qty){
            return res.status(400).json({error:"missing required feild"})
        }
        const menuItem=await prisma.menuitems.findUnique({
            where:{item_id:item_id}
        })
        if(!menuItem){
            return res.status(400).json({error:"item not found"})
        }
       
        const ifItemExistInCart= await prisma.cart.findFirst({
         
                where:{item_id:item_id,cart_id:cart_id}
            
        })
        if(ifItemExistInCart){
            // update
            const updatedItemRecord=await prisma.cart.update({
                where:{cart_item_id:ifItemExistInCart.cart_item_id},
                // data:{item_qty:ifItemExistInCart.item_qty+parseInt(item_qty)}
                data:{item_qty}
            })
            res.status(201).json(updatedItemRecord);
        }else{
            // add 
            const addedItemInCart= await prisma.cart.create({
                data:{
                 cart_id,item_id,item_qty
                } 
         })
         console.log(addedItemInCart);
         res.json(addedItemInCart); 

        }      
    } catch (error) {
        console.log(error);
    }
  
})
// get All addeditem in cart
router.get('/cartWithSameId/:id',async(req,res)=>{
    const {id} = req.params;
    const cartItems = await prisma.cart.findMany({
        where: {
            cart_id: parseInt(id),
        },
        include: {
            menuitems: {
                select: {
                    item_name: true,
                    item_price: true
                }
            }
        }
    })
    const allCartItems = cartItems.map(item => ({
        name: item.menuitems.item_name,
        quantity: item.item_qty,
        item_price: parseInt(item.menuitems.item_price),
        total_price: (item.item_qty * item.menuitems.item_price) 
    }))
    const finalResp = {
        cart_id: parseInt(id),
        items: allCartItems,
        totalCartPrice: allCartItems.reduce((acc, cur) => acc + cur.total_price, 0)
    }
    res.json(finalResp)
})
// delete single item from 
router.delete('/deleteCartItem/:id',async(req,res)=>{
    try {
        const deletedItem=await prisma.cart.delete({
            where:{cart_item_id:parseInt(req.params.id)}
        })
        console.log(deletedItem);
        res.json(deletedItem);
    } catch (error) {
        console.log(error);
    }
    
})
// delete all itm from cart 
router.delete('/emptyCart/:cart_id',async(req,res)=>{
    try {
        const allItems=await prisma.cart.deleteMany({
            where:{cart_id:parseInt(req.params.cart_id)}
        })
        console.log(allItems);
        res.json(allItems);
    } catch (error) {
        console.log(error);
    }
  
})

router.get('/getMaxCartId' , async(req,res)=>{
    try {
        const getMaxCartId=await prisma.cart.findFirst({
            select: {
                cart_id: true
            },
            orderBy: {
                cart_id: 'desc' // Ordering by cart_id in descending order
            },
            take: 1 
        })// Take only the first result
            if (getMaxCartId) {
                res.json({ getMaxCartId: getMaxCartId.cart_id });
            } else {
                res.status(404).json({ error: "No cart_id found" });
            }

        
    } catch (error) {
        console.log(error);
    }
})

module.exports=router;