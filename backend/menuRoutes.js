const express=require('express')
const router=express.Router();
const { PrismaClient } = require('@prisma/client');
const path = require('path');
const { log } = require('console');
require('dotenv').config({ path: path.resolve(__dirname, '../.env') });

const prisma = new PrismaClient();
// add menuitems
router.post('/menuitem',async (req,res)=>{
    try {
        const { item_name, item_type,item_description, item_imageurl,item_price } = req.body;

        const   newMenuitems = await prisma.menuitems.create({
            data: {
                
                item_name,
                item_type,
                item_description,
                item_price,
                item_imageurl
            },
        // res.send("hii")
          });
      
    res.json(newMenuitems);
    res.json()
   
    } catch (error) {
        console.log(error);
    }finally{
        await prisma.$disconnect();
    }
   
})
// get all menuitems
router.get('/menuitems', async (req, res) => {
    try {
      const menuitems = await prisma.menuitems.findMany();
      console.log({menuitems})
      const respData = menuitems.map((item) => {
        return {
            food_id: item.item_id,
            food_name: item.item_name,
            food_description: item.item_description,
            food_imageurl: item.item_imageurl,
            food_price: item.item_price,
            food_item_type: item.item_type,
        }
        
      })
      res.json(respData);
    } catch (error) {
      console.error('Error fetching menu items:', error);
      res.status(500).json({ error: 'Internal Server Error' });
    }
  });
// get menuitems by id
  router.get('/menuitems/:item_id',async (req,res)=>{
    try {
        const  menuitems= await prisma.menuitems.findFirst({
            where: { item_id: parseInt(req.params.item_id) },
        });
        res.json({
            food_id: menuitems.item_id,
            food_name: menuitems.item_name,
            food_description: menuitems.item_description,
            food_imageurl: menuitems.item_imageurl,
            food_price: menuitems.item_price,
            food_item_type: menuitems.item_type,
        });
        console.log(menuitems);

       
    } catch (error) {
        console.log(error);
        res.status(500).json({error:"interanl server error"})
    }
       
  })
  
// delete menuitems
  router.delete('/menuitems/:item_id',async (req,res)=>{
    try {
        const  menuitems= await prisma.menuitems.delete({
            where: { item_id: parseInt(req.params.item_id) },
        });
        res.json({menuitems});
        console.log(menuitems);


    } catch (error) {
        console.log(error);
        res.status(500).json({error:"interanl server error"})
    }
       
  })
//   update menuitems
router.put('/menuitems/:item_id',async(req,res)=>{
    try {
        const {item_name, item_type,item_description, item_imageurl,item_price}=req.body;
        const updatedItem=await prisma.menuitems.update({
            where:{item_id:parseInt(req.params.item_id)},
            data:{
                item_name, item_type,item_description, item_imageurl,item_price
            }
        })
        res.json(updatedItem);
        
    } catch (error) {
        console.log(error);
    }
   
})

module.exports=router;
